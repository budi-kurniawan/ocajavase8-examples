package app08;

public class CapitalizedSample {

    public static void main(String[] args) {
        StringUtil util = new StringUtil();
        String input = "Capitalize";
        try {
            String capitalized = util.capitalize(input);
            System.out.println(capitalized);
        } catch (NullPointerException e) {
            System.out.println(e.toString());
        } catch (AlreadyCapitalizedException e) {
            e.printStackTrace();
        }

    }

}
