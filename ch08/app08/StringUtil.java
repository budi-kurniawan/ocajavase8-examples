package app08;

public class StringUtil {
    public String capitalize(String s) throws AlreadyCapitalizedException {
        if (s.charAt(0) != s.toUpperCase().charAt(0)) {
            throw new AlreadyCapitalizedException();
        }
        return s.toUpperCase().charAt(0) + s.substring(1);
    }
}
