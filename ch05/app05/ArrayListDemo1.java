package app05;

import java.util.ArrayList;

public class ArrayListDemo1 {

    public static void main(String[] args) {
        ArrayList<String> myList = new ArrayList<>();
        myList.add("Hello");
        myList.add(1, "World");
        myList.add(null);
        System.out.println("Size: " + myList.size());
        for (String word: myList) {
            System.out.println(word);
        }

    }

}
